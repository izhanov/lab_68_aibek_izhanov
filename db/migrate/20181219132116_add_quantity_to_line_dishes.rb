class AddQuantityToLineDishes < ActiveRecord::Migration[5.2]
  def change
    add_column :line_dishes, :quantity, :integer, default: 1
  end
end
