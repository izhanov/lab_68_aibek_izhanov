class CreateLineDishes < ActiveRecord::Migration[5.2]
  def change
    create_table :line_dishes do |t|
      t.references :dish, foreign_key: true
      t.references :cart, foreign_key: true

      t.timestamps
    end
  end
end
