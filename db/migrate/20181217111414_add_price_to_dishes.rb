class AddPriceToDishes < ActiveRecord::Migration[5.2]
  def change
    add_column :dishes, :price, :decimal
  end
end
