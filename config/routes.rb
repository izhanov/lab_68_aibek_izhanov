Rails.application.routes.draw do
  get 'carts/show'
  post 'places/:place_id/:dish_id/add' => 'line_dishes#create', as: 'add_dish'

  ActiveAdmin.routes(self)
  root 'places#index'
  devise_for :users
  resources :places, only: [:index, :show]
end
