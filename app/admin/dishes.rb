ActiveAdmin.register Dish do
  permit_params :name, :description, :price, :image, :place_id

    form do |f|
      f.inputs do
        f.input :name
        f.input :price
        f.input :description
        f.input :place_id, :as => :select, collection: Place.all.map { |m| [m.name, m.id] },
                selected: object.place_id
        if f.object.image.attached?
          f.input :image,
            :as => :file,
            :hint => image_tag(
              url_for(
                f.object.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
                )
              )
        else
              f.input :image, :as => :file
        end
      end
      f.actions
    end

    index do
        selectable_column
        id_column
        column :image do |dish|
          image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
        end
        column :title do |dish|
          link_to dish.name, admin_dish_path(dish)
        end
        actions
    end

    show do
      attributes_table do
        row :image do |dish|
          image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
        end
        row :name
        row :price
        row :description
        row :place
      end
      active_admin_comments
     end

end
