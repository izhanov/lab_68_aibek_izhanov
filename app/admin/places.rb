ActiveAdmin.register Place do
permit_params :name, :description, :picture

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      if f.object.picture.attached?
        f.input :picture,
          :as => :file,
          :hint => image_tag(
            url_for(
              f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
              )
            )
      else
            f.input :picture, :as => :file
      end
    end
    f.actions
  end

  index do
      selectable_column
      id_column
      column :picture do |place|
        image_tag place.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
      end
      column :title do |place|
        link_to place.name, admin_place_path(place)
      end
      actions
  end

  show do
    attributes_table do
      row :picture do |place|
        image_tag place.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
      end
      row :name
      row :description
    end
    active_admin_comments
   end

end
