class LineDishesController < ApplicationController
  before_action :current_cart

  def create

    chosen_product = Dish.find(params[:dish_id])
    current_cart = @current_cart

    if current_cart.dishes.include?(chosen_product)
      @line_dish = current_cart.line_dishes.find_by(:dish_id => chosen_product)
      @line_dish.quantity += 1
    else
      @line_dish = LineDish.new
      @line_dish.cart = current_cart
      @line_dish.dish = chosen_product
    end


    @line_dish.save
    redirect_to place_path(Place.find(params[:place_id]).id)
  end


  def destroy
    

  end



end
