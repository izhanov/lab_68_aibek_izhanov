class CartsController < ApplicationController
  before_action :set_place
  def show
    @cart = @current_cart
    @cart.place_id = @place.id
    @cart.save
  end

  def destroy
    @cart = @current_cart
    @cart.destroy
    session[:cart_id] = nil
    redirect_to root_path
  end
  private
  def set_place
    @place = Place.find(params[:place_id])
  end
end
