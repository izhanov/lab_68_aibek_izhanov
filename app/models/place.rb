class Place < ApplicationRecord
  has_one_attached :picture
  validates :picture,
    file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

  has_many :dishes, dependent: :destroy

  has_one :cart
end
