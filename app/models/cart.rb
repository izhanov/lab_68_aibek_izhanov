class Cart < ApplicationRecord

  has_many :line_dishes, dependent: :destroy
  has_many :dishes, through: :line_dishes




  def sub_total
    sum = 0

    self.line_dishes.each do |line_dish|
      if line_dish.dish.place.id == self.place_id
        sum+= line_dish.total_price
      end
    end
    return sum
  end
end
