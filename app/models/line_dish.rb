class LineDish < ApplicationRecord
  belongs_to :dish
  belongs_to :cart

  def total_price
    self.quantity * self.dish.price
  end

end
