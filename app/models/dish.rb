class Dish < ApplicationRecord
  validates :price, presence: true,
            numericality: {greater_than_or_equal_to: 0}
  has_one_attached :image
  validates :image,
        file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

  has_many :line_dishes

  belongs_to :place
end
